package cz.principiasolutions.model;

/**
 * Created by martin on 26.2.17.
 */
public class Rating {

    int itemId;

    int rating;

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Rating() {
    }

    public Rating(int itemId, int rating) {
        this.itemId = itemId;
        this.rating = rating;
    }
}
