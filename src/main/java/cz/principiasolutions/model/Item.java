package cz.principiasolutions.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by martin on 25.2.17.
 */
@Document(collection="movies")
public class Item {


    String item_id;

    String item_name;

    String item_url;

    public Item(String id, String name, String url) {
        this.item_id = id;
        this.item_name = name;
        this.item_url = url;
    }

    public Item() {
    }

    public String getItem_id() {
        return item_id;
    }


    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getItem_url() {
        return item_url;
    }

    public void setItem_url(String item_url) {
        this.item_url = item_url;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + item_id +
                ", name='" + item_name + '\'' +
                ", url='" + item_url + '\'' +
                '}';
    }
}
