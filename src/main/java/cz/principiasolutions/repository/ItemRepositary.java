package cz.principiasolutions.repository;

import cz.principiasolutions.model.Item;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by martin on 2.3.17.
 */
public interface ItemRepositary extends MongoRepository<Item, String> {


}
