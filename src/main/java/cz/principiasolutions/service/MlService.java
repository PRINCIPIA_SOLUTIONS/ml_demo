package cz.principiasolutions.service;

import cz.principiasolutions.model.Item;
import cz.principiasolutions.model.Rating;
import cz.principiasolutions.repository.ItemRepositary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by martin on 26.2.17.
 */
@Component
public class MlService {

    @Autowired
    private ItemRepositary itemRepositary;

    public List<Item> processData(List<Rating> ratings) throws IOException {

        //String command = "python3 /home/martin/PycharmProjects/ml_solvers/colaborative_filtering/colaborative_filtering_user.py ";
        String command = "python3 /home/dp/colaborative_filtering_user.py ";

        StringBuilder sb = new StringBuilder();

        for (Rating rating : ratings) {
            sb.append(rating.getItemId());
            sb.append(":");
            sb.append(rating.getRating());
            sb.append(",");
        }
        sb.deleteCharAt(sb.lastIndexOf(","));

        Process p = Runtime.getRuntime().exec(command+sb);
        BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));

        StringBuffer output = new StringBuffer();

        String line;
        while ((line = in.readLine()) != null) {
            output.append(line + "\n");
        }

        List<Item> allItems = itemRepositary.findAll();

        List<String> split = Arrays.asList(output.toString().trim().replace("[", "").replace("]", "").replace(" ", "").replace("\n", "").split(","));

        List<Item> items = allItems
                .stream()
                .filter(item -> split.contains(item.getItem_id()))
                .collect(Collectors.toList());

        return items;
    }

}
