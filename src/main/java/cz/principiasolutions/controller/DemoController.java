package cz.principiasolutions.controller;

import cz.principiasolutions.model.Item;
import cz.principiasolutions.model.Rating;
import cz.principiasolutions.repository.ItemRepositary;
import cz.principiasolutions.service.MlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Created by martin on 25.2.17.
 */
@Controller
public class DemoController {

    @Autowired
    MlService mlService;

    @Autowired
    private ItemRepositary itemRepositary;

    @RequestMapping("/")
    public String getIndex(Model model) {
        model.addAttribute("items", itemRepositary.findAll(new Sort(new Sort.Order(Sort.Direction.ASC, "item_name"))));
        return "index";
    }

    @RequestMapping("/main")
    public String getMainPage(Model model) {
        model.addAttribute("items", itemRepositary.findAll(new Sort(new Sort.Order(Sort.Direction.ASC, "item_name"))));
        return "main";
    }

    @RequestMapping("/result")
    public String getPrediction(@RequestParam Map<String,String> requestParams, Model model) {

        ArrayList<Rating> ratings = new ArrayList<>();
        requestParams.forEach((k, v) -> {
            ratings.add(new Rating(Integer.parseInt(k), Integer.parseInt(v)));
        });

        List<Item> items = null;

        try {
            items = mlService.processData(ratings);
        } catch (IOException e) {
            e.printStackTrace();
        }

        model.addAttribute("items", items);
        return "result";
    }
}